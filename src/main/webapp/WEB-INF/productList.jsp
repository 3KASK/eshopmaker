<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>eShopMaker</title>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.common.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.default.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="<%=request.getContextPath()%>/resource/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resource/js/kendo/kendo.web.min.js"></script>
</head>
<body>

<jsp:include page="/WEB-INF/menu.jsp"/>
<div id="example">
    <h3> Lista produktow </h3>

    </br>
    <button id="AddProduct" class="k-primary" style="margin:5px; zoom:110%">Dodaj nowy produkt</button>
    <div id="products" style="zoom:90%"></div>

</div>
</body>
</html>
<script>
    $(document).ready(function () {

        var products = [
            {"Id": 1, "Name": "DELL Inspiron 3542 [0792] - 120GB SSD", "Category": "Laptopy", "Price": 1999},
            {"Id": 2, "Name": "Lenovo Y70-70 (80DU00MQPB)", "Category": "Laptopy", "Price": 4299},
            {"Id": 3, "Name": "Microsoft Surface 3", "Category": "Laptopy", "Price": 1999},
            {"Id": 4, "Name": "Acer Aspire S7", "Category": "Laptopy", "Price": 4799},
            {"Id": 5, "Name": "DELL XPS Duo 12", "Category": "Laptopy", "Price": 3799},
            {"Id": 6, "Name": "Intel Core i5-4460", "Category": "Procesory", "Price": 859},
            {"Id": 7, "Name": "Intel Core i7-4820K", "Category": "Procesory", "Price": 1540},
            {"Id": 8, "Name": "AMD X8 FX-8320", "Category": "Procesory", "Price": 739},
            {"Id": 9, "Name": "Noctua NH-U12P SE2", "Category": "Chlodzenie", "Price": 279},
            {"Id": 10, "Name": "Noctua NH-C12P SE14", "Category": "Chlodzenie", "Price": 329},
            {"Id": 11, "Name": "ProlimaTech Megahalems Rev.C", "Category": "Chlodzenie", "Price": 249},
            {"Id": 12, "Name": "be quiet! Shadow Rock TopFlow", "Category": "Chlodzenie", "Price": 239},
            {"Id": 13, "Name": "Razer Blackwidow Chroma", "Category": "Peryferia", "Price": 799},
            {
                "Id": 14,
                "Name": "CALIFORNIA ACCESS klawiatura CA-1404 T-REX mechaniczna gamingowa",
                "Category": "Peryferia",
                "Price": 330
            },
            {"Id": 15, "Name": "Logitech K270", "Category": "Peryferia", "Price": 99},
            {"Id": 16, "Name": "Microsoft Arc Keyboard", "Category": "Peryferia", "Price": 189}
        ];
        $("#products").kendoGrid({
            dataSource: {
                data: products,
                schema: {
                    model: {
                        fields: {
                            Id: {type: "number"},
                            Name: {type: "string"},
                            Category: {type: "string"},
                            Price: {type: "number"}
                        }
                    }
                },
                pageSize: 10
            },
            height: 550,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {field: "Id", width: "100px"},
                {field: "Name"},
                {field: "Category"},
                {field: "Price"},
                {command: {text: "Szczegoly", click: showDetails}, title: " ", width: "180px"},
                {command: {text: "Usun", click: showDetails}, title: " ", width: "180px"}],
        });
    });

    function showDetails(e) {
        e.preventDefault();

        // tu przekierowanie do widoku szczegolow produktu
        // var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        // wnd.content(detailsTemplate(dataItem));
        // wnd.center().open();
    }
</script>
