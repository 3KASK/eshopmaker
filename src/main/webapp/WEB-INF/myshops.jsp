<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>eShopMaker</title>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.common.min.css" rel="stylesheet" type="text/css"/>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.default.min.css" rel="stylesheet" type="text/css"/>
    <script src="<%=request.getContextPath()%>/resource/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resource/js/kendo/kendo.web.min.js"></script>
</head>
<body>

<jsp:include page="/WEB-INF/menu.jsp"/>

<div id="example">
    <h3> Moje sklepy </h3>
    </br>
    <button id="AddShop" class="k-primary" style="margin:5px; zoom:110%" onclick="openNewShopDialog()">Stworz nowy sklep
    </button>
    <div id="myshops" style="zoom:90%"></div>

    <div id="new-shop-dialog" style="display:none">
        Nazwa sklepu: <input id="new-shop-name" type="text"></input>
        <button onclick="addNewShop()">Stworz nowy sklep</button>
    </div>
</div>


</body>
</html>
<script>

    openNewShopDialog = function() {
        $("#new-shop-dialog").kendoWindow({
            width: "150px",
            height: "100px",
            title: "Stwórz nowy sklep",
            visible: false
        }).data("kendoWindow").center().open();
    };

    addNewShop = function() {
        $("#new-shop-dialog").data("kendoWindow").close();
        $.ajax({
            type: 'POST',
            url: '<%=request.getContextPath()%>/api/user/admin/shop',
            data: JSON.stringify({"shopName": $("#new-shop-name").val()}),
            success: function(data) {
                reloadGrid();
            },
            contentType: "application/json",
            dataType: 'json'
        });
    };

    reloadGrid = function() {
        $.get("<%=request.getContextPath()%>/api/user/admin/shops", function (data, status) {
            $("#myshops").kendoGrid({
                dataSource: {
                    data: data,
                    schema: {
                        model: {
                            fields: {
                                id: {type: "string"},
                                shopName: {type: "string"},

                            }
                        }
                    },
                    pageSize: 10
                },
                height: 550,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {field: "id", width: "300px"},
                    {field: "shopName", title: "Nazwa", width: "300px"},
                    {command: {text: "Przejdz do sklepu", click: showDetails}, title: " ", width: "180px"}]
            });
        });
    };

    $(document).ready(function () {
        reloadGrid();
    });

    function showDetails(e) {
        e.preventDefault();

        var grid = $("#myshops").data("kendoGrid");
        var shopGridItem = grid.dataItem(grid.tbody.find(">tr:first"));

        window.location = '<%=request.getContextPath()%>/shop/' + shopGridItem.id;

        // tu przekierowanie do widoku sklepu
        // var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        // wnd.content(detailsTemplate(dataItem));
        // wnd.center().open();
    }
</script>

<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0, 0, 0, .2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>