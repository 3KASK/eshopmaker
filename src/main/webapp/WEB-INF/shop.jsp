<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>eShopMaker</title>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.common.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.default.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="<%=request.getContextPath()%>/resource/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resource/js/kendo/kendo.web.min.js"></script>
</head>
<body>

<jsp:include page="/WEB-INF/menu.jsp"/>
<div id="example">
    <h3> Sklep "The Goath" </h3>
    <h4> Zeby skonfigurowac swoj sklep zlap wybrane komponenty z lewej strony i przestaw je lub przeciagnij na prawa
        strone. </h4>
    </br>
    <button id="save" class="k-primary" style="margin:5px; zoom:110%">Zapisz</button>
    <div id="example">

        <div class="panel-wrap hidden-on-narrow">
            <div id="sidebar">
                <div id="comments" class="widget">
                    <h3>Ostatnie komentarze</h3>
                    <div>
                        lista komentarzy
                    </div>
                </div>

                <div id="koszyk" class="widget">
                    <h3>koszyk</h3>
                    <div>
                        <img src="<%=request.getContextPath()%>/resource/image/cart.png" style="width: 100px">
                    </div>
                </div>

                <div id="kontakt" class="widget">
                    <h3>kontakt</h3>
                    <div>
                        kontakt info
                    </div>
                </div>

                <div id="fb" class="widget">
                    <h3>fb</h3>
                    <div>
                        Polub nas na fb
                        <img src="<%=request.getContextPath()%>/resource/image/fb.jpg" style="width: 100px">
                    </div>

                </div>

                <div id="polecane" class="widget">
                    <h3>polecane produkty</h3>
                    <div>
                        w tym tygodzniu polecamy
                    </div>
                </div>


            </div>
            <div id="main-content">

                <div id="products" class="widget">
                    <h3>Produkty</h3>

                    <div id="productsl" style="zoom:70%"></div>

                </div>

            </div>
        </div>

        <div class="responsive-message"></div>

        <script>
            $(document).ready(function () {

                $("#sidebar").kendoSortable({
                    filter: ">div",
                    cursor: "move",
                    connectWith: "#main-content",
                    placeholder: placeholder,
                    hint: hint
                });

                $("#main-content").kendoSortable({
                    filter: ">div",
                    cursor: "move",
                    connectWith: "#sidebar",
                    placeholder: placeholder,
                    hint: hint
                });

                //exapand
                $(".panel-wrap").on("click", "span.k-i-arrowhead-s", function (e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                            .removeClass("k-i-arrowhead-s")
                            .addClass("k-i-arrowhead-n");

                    kendo.fx(contentElement).expand("vertical").stop().play();
                });

                //collapse
                $(".panel-wrap").on("click", "span.k-i-arrowhead-n", function (e) {
                    var contentElement = $(e.target).closest(".widget").find(">div");
                    $(e.target)
                            .removeClass("k-i-arrowhead-n")
                            .addClass("k-i-arrowhead-s");

                    kendo.fx(contentElement).expand("vertical").stop().reverse();
                });
                var products = [
                    {"Id": 1, "Name": "DELL Inspiron 3542 [0792] - 120GB SSD", "Category": "Laptopy", "Price": 1999},
                    {"Id": 2, "Name": "Lenovo Y70-70 (80DU00MQPB)", "Category": "Laptopy", "Price": 4299},
                    {"Id": 3, "Name": "Microsoft Surface 3", "Category": "Laptopy", "Price": 1999},
                    {"Id": 4, "Name": "Acer Aspire S7", "Category": "Laptopy", "Price": 4799},
                    {"Id": 5, "Name": "DELL XPS Duo 12", "Category": "Laptopy", "Price": 3799},
                    {"Id": 6, "Name": "Intel Core i5-4460", "Category": "Procesory", "Price": 859},
                    {"Id": 7, "Name": "Intel Core i7-4820K", "Category": "Procesory", "Price": 1540},
                    {"Id": 8, "Name": "AMD X8 FX-8320", "Category": "Procesory", "Price": 739},
                    {"Id": 9, "Name": "Noctua NH-U12P SE2", "Category": "Chlodzenie", "Price": 279},
                    {"Id": 10, "Name": "Noctua NH-C12P SE14", "Category": "Chlodzenie", "Price": 329},
                    {"Id": 11, "Name": "ProlimaTech Megahalems Rev.C", "Category": "Chlodzenie", "Price": 249},
                    {"Id": 12, "Name": "be quiet! Shadow Rock TopFlow", "Category": "Chlodzenie", "Price": 239},
                    {"Id": 13, "Name": "Razer Blackwidow Chroma", "Category": "Peryferia", "Price": 799},
                    {
                        "Id": 14,
                        "Name": "CALIFORNIA ACCESS klawiatura CA-1404 T-REX mechaniczna gamingowa",
                        "Category": "Peryferia",
                        "Price": 330
                    },
                    {"Id": 15, "Name": "Logitech K270", "Category": "Peryferia", "Price": 99},
                    {"Id": 16, "Name": "Microsoft Arc Keyboard", "Category": "Peryferia", "Price": 189}
                ];
                $("#productsl").kendoGrid({
                    dataSource: {
                        data: products,
                        schema: {
                            model: {
                                fields: {
                                    Id: {type: "number"},
                                    Name: {type: "string"},
                                    Category: {type: "string"},
                                    Price: {type: "number"}
                                }
                            }
                        },
                        pageSize: 10
                    },
                    height: 550,
                    sortable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [
                        {field: "Id", width: "100px"},
                        {field: "Name"},
                        {field: "Category"},
                        {field: "Price"}],
                });
            });

            function showDetails(e) {
                e.preventDefault();

                // tu przekierowanie do widoku szczegolow produktu
                // var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                // wnd.content(detailsTemplate(dataItem));
                // wnd.center().open();
            }

            function placeholder(element) {
                return element.clone().addClass("placeholder");
            }

            function hint(element) {
                return element.clone().addClass("hint")
                        .height(element.height())
                        .width(element.width());
            }
        </script>

        <style>
            #example {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .dash-head {
                width: 970px;
                height: 80px;
                background: url('../content/web/sortable/dashboard-head.png') no-repeat 50% 50% #222222;
            }

            .panel-wrap {
                display: table;
                margin: 0 0 20px;
                width: 968px;
                background-color: #f5f5f5;
                border: 1px solid #e5e5e5;
            }

            #sidebar {
                display: table-cell;
                margin: 0;
                padding: 20px 0 20px 20px;
                width: 220px;
                vertical-align: top;
            }

            #main-content {
                display: table-cell;
                margin: 0;
                padding: 20px;
                width: 680px;
                vertical-align: top;
            }

            .widget.placeholder {
                opacity: 0.4;
                border: 1px dashed #a6a6a6;
            }

            /* WIDGETS */
            .widget {
                margin: 0 0 20px;
                padding: 0;
                background-color: #ffffff;
                border: 1px solid #e7e7e7;
                border-radius: 3px;
                cursor: move;
            }

            .widget:hover {
                background-color: #fcfcfc;
                border-color: #cccccc;
            }

            .widget div {
                padding: 10px;
                min-height: 50px;
            }

            .widget h3 {
                font-size: 12px;
                padding: 8px 10px;
                text-transform: uppercase;
                border-bottom: 1px solid #e7e7e7;
            }

            .widget h3 span {
                float: right;
            }

            .widget h3 span:hover {
                cursor: pointer;
                background-color: #e7e7e7;
                border-radius: 20px;
            }
        </style>
    </div>
</body>
</html>
