<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Language" content="pl"/>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>eShopMaker</title>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.common.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<%=request.getContextPath()%>/resource/style/kendo/kendo.default.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="<%=request.getContextPath()%>/resource/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resource/js/kendo/kendo.web.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/menu.jsp"/>
<div id="example">
    <div id="vertical">
        <div id="top-pane">
            <div id="horizontal" style="height: 100%; width: 100%;">
                <div id="left-pane">
                    <div class="pane-content">
                        <h3>Produkt</h3>
                        <img src="<%=request.getContextPath()%>/resource/image/dellProd.png"
                             style="width:90%; height:90%; float:left; margin:30px;">
                        </br>
                        <h3>Cena</h3>
                        <h2>1499,99zł</h2>
                    </div>
                </div>
                <div id="center-pane">
                    <div class="pane-content">
                        <h3>Galeria</h3>
                        <img src="<%=request.getContextPath()%>/resource/image/dell.png"
                             style="width: 370px; height: 200px; float:left; margin:10px;">
                        <img src="<%=request.getContextPath()%>/resource/image/dell3.jpg"
                             style="width: 370px; height: 200px; float:left; margin:10px;">
                        <img src="<%=request.getContextPath()%>/resource/image/dell4.jpeg"
                             style="width: 370px; height: 200px; float:left; margin:10px;">
                    </div>
                </div>
                <div id="right-pane">
                    <div class="pane-content">
                        <h3>Opis</h3>
                        <p><b>Dell Inspiron 3542 </b><br/>
                            Inspiron 3542 ma prosta obudowe z czarnego tworzywa. Wiekszosc jej elementow jest matowa.
                            Tylko boki jednostki zasadniczej i pokrywy ekranu sa polyskliwe. Plecy pokrywy ekranu sa
                            pokryte wzorkiem nadajacym chropowatosc fakturze tej czesci laptopa.

                            Jezeli chodzi o sztywnosc i jakosc wykonania, sa pewne mankamenty, ale nie sa to wady
                            dyskwalifikujace. Jednostka zasadnicza ugina sie pod naciskiem na lewo od klawiatury. Poza
                            tym jednostka zasadnicza daje sie zbyt mocno wykrzywiac. To samo tyczy sie pokrywy ekranu,
                            przy czym jednoczesnie z jej wykrzywianiem slychac ciche trzeszczenie. W czasie wykrzywiania
                            pokrywy ekranu i naciskania od tylu na wieko wystepuja zaburzenia w wyswietlanym obrazie.

                            Zawiasy pozwalaja na chybotanie sie ekranu po zmianie jego ustawienia.

                            Akumulator nie jest wbudowany na stale.

                            Inspiron 3542 ma pokrywe serwisowa, ktora jest przymocowana tylko jedna srubka. Po jej
                            zdemontowaniu uzytkownik ma dostep do dysku twardego, pamieci RAM (omawiany laptop ma tylko
                            jedno gniazdo tejze) i karty Wi-Fi.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="middle-pane" style="height:300px">
            <div class="pane-content">
                <h3>Opinie</h3>
                <div class="CSSTableGenerator">
                    <table>
                        <tr>
                            <td>
                                <img src="<%=request.getContextPath()%>/resource/image/5star.png" style="width: 200px">
                            </td>
                            <td>
                                Spelnia wszystkie moje oczekiwania, polecam:)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="<%=request.getContextPath()%>/resource/image/5star.png" style="width: 200px">
                            </td>
                            <td>
                                w tej cenie trudno kupic cos lepszego.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="<%=request.getContextPath()%>/resource/image/5star.png" style="width: 200px">
                            </td>
                            <td>
                                Dobry laptop za taka cene
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#vertical").kendoSplitter({
                orientation: "vertical",
                panes: [
                    {collapsible: false},
                    {collapsible: false, size: "300px"},
                    {collapsible: false, resizable: false, size: "100px"}
                ]
            });

            $("#horizontal").kendoSplitter({
                panes: [
                    {collapsible: true},
                    {collapsible: false},
                    {collapsible: true}
                ]
            });
        });
    </script>
</div>

</body>
</html>

<style>
    #vertical {
        height: 850px;
        margin: 0 auto;
    }

    #middle-pane {
        background-color: rgba(60, 70, 80, 0.05);
    }

    #left-pane, #center-pane, #right-pane {
        background-color: rgba(60, 70, 80, 0.05);
    }

    .pane-content {
        padding: 0 10px;
    }

    .CSSTableGenerator {
        margin: 0px;
        padding: 0px;
        width: 100%;
        border: 1px solid #ffffff;

    }

    .CSSTableGenerator table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    }
    .CSSTableGenerator tr:nth-child(odd) {
        background-color: #cccccc;
    }

    .CSSTableGenerator tr:nth-child(even) {
        background-color: #ffffff;
    }

    .CSSTableGenerator td {
        vertical-align: middle;

        border: 1px solid #ffffff;
        border-width: 0px 1px 1px 0px;
        text-align: left;
        padding: 7px;
        font-size: 10px;
        font-family: Arial;
        font-weight: normal;
        color: #000000;
    }

    .CSSTableGenerator tr:last-child td {
        border-width: 0px 1px 0px 0px;
    }

    .CSSTableGenerator tr td:last-child {
        border-width: 0px 0px 1px 0px;
    }

    .CSSTableGenerator tr:last-child td:last-child {
        border-width: 0px 0px 0px 0px;
    }
</style>