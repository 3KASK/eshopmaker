<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div id="megaStore">
<kendo:menu name="Menu">
    <kendo:menu-items>
        <kendo:menu-item text="Strona główna"></kendo:menu-item>
        <kendo:menu-item text="Komponenty">
            <kendo:menu-items>
                <kendo:menu-item text="Lista produktów">
                    <kendo:menu-items>
                        <kendo:menu-item text="Wyszukiwarka"></kendo:menu-item>
                        <kendo:menu-item text="Filtrowanie"></kendo:menu-item>
                        <kendo:menu-item text="Kategorie"></kendo:menu-item>
                        <kendo:menu-item text="Porównywanie"></kendo:menu-item>
                    </kendo:menu-items>
                </kendo:menu-item>
                <kendo:menu-item text="Produkty">
                    <kendo:menu-items>
                        <kendo:menu-item text="Bestsellery"></kendo:menu-item>
                        <kendo:menu-item text="Opinie"></kendo:menu-item>
                        <kendo:menu-item text="Opis/Cechy"></kendo:menu-item>
                        <kendo:menu-item text="Zdjęcia"></kendo:menu-item>
                        <kendo:menu-item text="Dostępność"></kendo:menu-item>
                        <kendo:menu-item text="Polecane"></kendo:menu-item>
                    </kendo:menu-items>
                </kendo:menu-item>
                <kendo:menu-item text="Kontakt ze sprzedawcą">
                    <kendo:menu-items>
                        <kendo:menu-item text="Email"></kendo:menu-item>
                        <kendo:menu-item text="Livechat"></kendo:menu-item>
                        <kendo:menu-item text="FAQ"></kendo:menu-item>
                        <kendo:menu-item text="Formularz kontaktowy"></kendo:menu-item>
                    </kendo:menu-items>
                </kendo:menu-item>
                <kendo:menu-item text="Dodatki">
                    <kendo:menu-items>
                        <kendo:menu-item text="Koszyk"></kendo:menu-item>
                        <kendo:menu-item text="Newsletter"></kendo:menu-item>
                        <kendo:menu-item text="Konto użytkownika"></kendo:menu-item>
                        <kendo:menu-item text="Płatności online"></kendo:menu-item>
                        <kendo:menu-item text="Dostawa"></kendo:menu-item>
                    </kendo:menu-items>
                </kendo:menu-item>
            </kendo:menu-items>
        </kendo:menu-item>
        <kendo:menu-item text="Moje sklepy"></kendo:menu-item>
        <kendo:menu-item text="Moje konto"></kendo:menu-item>
        <kendo:menu-item text="Wyloguj się"></kendo:menu-item>
    </kendo:menu-items>
</kendo:menu>
</div>

<style>
    #megaStore {
        width: 600px;
        margin: 30px auto;
        padding-top: 10px;
        background: url('../resources/web/menu/header.jpg') no-repeat 0 0;
    }
    #menu h2 {
        font-size: 1em;
        text-transform: uppercase;
        padding: 5px 10px;
    }
    #template img {
        margin: 5px 20px 0 0;
        float: left;
    }
    #template {
        width: 380px;
    }
    #template ol {
        float: left;
        margin: 0 0 0 30px;
        padding: 10px 10px 0 10px;
    }
    #template:after {
        content: ".";
        display: block;
        height: 0;
        clear: both;
        visibility: hidden;
    }
    #template .k-button {
        float: left;
        clear: left;
        margin: 5px 0 5px 12px;
    }
</style>