package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api.shop;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * API for customer accounts
 */
@Path("shop/{shop}/user")
public class ShopAccountApi {

    @PathParam("shop")
    private String shop;

    @GET
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public String userProfile(@PathParam("username") String username) {
        return username;
    }
}
