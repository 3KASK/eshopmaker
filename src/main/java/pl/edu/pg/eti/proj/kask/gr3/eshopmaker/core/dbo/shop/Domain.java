package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Domain extends IdEntity {

    @ManyToOne
    public Shop shop;

    public String url;

    public boolean approved;
}
