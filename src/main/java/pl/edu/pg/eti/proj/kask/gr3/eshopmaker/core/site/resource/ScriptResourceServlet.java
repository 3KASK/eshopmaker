package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site.resource;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util.ResourceUtil;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util.StreamUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@WebServlet(name = "ScriptResourceServlet", value = "/resource/js/*")
public class ScriptResourceServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String queriedResource = request.getPathInfo();
        String resolvedPath = ResourceUtil.getAbsolutePath("js" + queriedResource);
        if (!resolvedPath.isEmpty()) {
            StreamUtil.writeFileToOutputStream(resolvedPath, response.getOutputStream());
        } else {
            response.sendError(SC_NOT_FOUND);
        }
    }
}