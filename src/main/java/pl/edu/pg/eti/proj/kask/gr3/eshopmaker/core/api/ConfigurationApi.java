package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * API for platform configuration management
 */
@Path("config")
public class ConfigurationApi {

    @GET
    public String config(){
        return "config";
    }
}
