package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api.shop;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * API for shop authentication
 */
@Path("shop/{shop}/auth")
public class ShopAuthenticationApi {

    @PathParam("shop")
    private String shop;
}
