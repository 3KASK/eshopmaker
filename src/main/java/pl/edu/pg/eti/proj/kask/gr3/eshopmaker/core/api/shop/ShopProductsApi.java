package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api.shop;

import javax.ws.rs.GET;  
import javax.ws.rs.Path;  
import javax.ws.rs.PathParam;

/**
 * API for shop products
 */
@Path("shop/{shop}/products")
public class ShopProductsApi {

    @PathParam("shop")
    private String shop;

    @GET
    public String products(){
        return "products";
    }
}