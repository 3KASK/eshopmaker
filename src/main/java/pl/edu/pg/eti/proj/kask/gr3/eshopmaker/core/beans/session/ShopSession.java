package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.session;

import java.io.Serializable;

public class ShopSession implements Serializable {

    public String hashKey;

    public boolean isLoggedIn;
    public String username;

    public long logoutTime;
    public long loginTime;

}
