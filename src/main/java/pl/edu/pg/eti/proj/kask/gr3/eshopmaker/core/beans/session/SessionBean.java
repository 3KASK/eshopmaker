package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.session;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SessionScoped
public class SessionBean implements Serializable {

    private Map<String, ShopSession> sessionMap;

    @PostConstruct
    public void postConstruct() {
        sessionMap = new HashMap<>();
    }

    public ShopSession getSession(String shopId) {
        return sessionMap.get(shopId);
    }
}
