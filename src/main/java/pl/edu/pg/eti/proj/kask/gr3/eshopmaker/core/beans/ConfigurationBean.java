package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Reads static configuration from configuration files in home directory
 */
@ApplicationScoped
public class ConfigurationBean {

    @Inject
    private Logger logger;

    @Inject
    private HomeDirectoryBean homeDirectoryBean;

    @PostConstruct
    public void postConstruct() {
        logger.info("constructed!");
    }

    public String getPlatformDomain() {
        return null;
    }
}
