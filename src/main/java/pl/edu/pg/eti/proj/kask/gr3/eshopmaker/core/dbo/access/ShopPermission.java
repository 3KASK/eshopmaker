package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.access;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Shop;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ShopPermission extends IdEntity {

    @ManyToOne
    public Account account;

    @ManyToOne
    public Shop shop;

    public int level;
}
