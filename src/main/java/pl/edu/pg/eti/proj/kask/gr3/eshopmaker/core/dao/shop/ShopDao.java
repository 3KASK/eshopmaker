package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.AbstractDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Shop;

public class ShopDao extends AbstractDao<Shop> {
}
