package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Property extends IdEntity {

    @ManyToOne
    public IdEntity target;

    public String key;

    public String value;

    public int security;
}
