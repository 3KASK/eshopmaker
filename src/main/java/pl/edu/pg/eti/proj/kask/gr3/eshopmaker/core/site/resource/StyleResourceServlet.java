package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site.resource;

import org.lesscss.LessCompiler;
import org.lesscss.LessException;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util.ResourceUtil;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util.StreamUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@WebServlet(name = "StyleResourceServlet", value = "/resource/style/*")
public class StyleResourceServlet extends HttpServlet {

    private LessCompiler lessCompiler;

    public StyleResourceServlet() {
        lessCompiler = new LessCompiler();
    }

    private String resolveResourcePath(String resourceName) {
        String resourcePath = ResourceUtil.getAbsolutePath("styles" + resourceName);
        if (resourcePath.isEmpty() && resourceName.endsWith(".css")) {
            resourceName = resourceName.substring(0, resourceName.length() - 4) + ".less";
            resourcePath = ResourceUtil.getAbsolutePath("styles" + resourceName);;
        }
        return resourcePath;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String queriedResource = request.getPathInfo();
        String resolvedPath = resolveResourcePath(queriedResource);
        if (!resolvedPath.isEmpty()) {
            if (resolvedPath.endsWith(".less")) {
                try {
                    response.getWriter().print(lessCompiler.compile(new File(resolvedPath)));
                } catch (LessException e) {
                    e.printStackTrace();
                    response.sendError(SC_NOT_FOUND);
                }
            } else {
                StreamUtil.writeFileToOutputStream(resolvedPath, response.getOutputStream());
            }
        } else {
            response.sendError(SC_NOT_FOUND);
        }
    }
}
