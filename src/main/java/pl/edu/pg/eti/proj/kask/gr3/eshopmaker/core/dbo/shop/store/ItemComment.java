package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.store;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.Content;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Customer;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ItemComment extends IdEntity {

    @ManyToOne
    public Customer author;

    @ManyToOne
    public Content content;
}
