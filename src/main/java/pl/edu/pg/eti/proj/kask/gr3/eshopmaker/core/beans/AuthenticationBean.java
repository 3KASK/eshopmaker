package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.access.AccountDao;

import javax.inject.Inject;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AuthenticationBean {

    @Inject
    DatabaseBean databaseBean;

    public boolean credentialsValid(final String username, final String passhash, final String hashKey) {
        AccountDao accountDao = databaseBean.getAccountDao();

        return accountDao.credentialsValid(username, passhash, (String password) -> {
            try {
                MessageDigest md5 = MessageDigest.getInstance("md5");

                md5.reset();
                md5.update((hashKey + password).getBytes());
                String hashFromPassword = new BigInteger(1, md5.digest()).toString(16);
                while (hashFromPassword.length() < 32) {
                    hashFromPassword = "0" + hashFromPassword;
                }

                return hashFromPassword;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return "";
            }
        });
    }
}
