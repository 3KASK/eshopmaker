package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.layout;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Shop;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Component extends IdEntity {

    @ManyToOne
    public Shop shop;

    public String name;
}
