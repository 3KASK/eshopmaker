package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.AuthenticationBean;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * API for platform authentication
 */
@Path("auth")
public class AuthenticationApi {

    public static final String AUTH_TILL_SESSION_KEY = "authTill";
    public static final String HASH_KEY_SESSION_KEY = "hashkey";
    public static final String USERNAME_SESSION_KEY = "username";

    @Inject
    private AuthenticationBean authenticationBean;

    public class AuthenticationResult {
        public boolean loggedIn;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AuthenticationResult authenticate(
            @Context HttpServletRequest request,
            @FormParam("username") String username,
            @FormParam("passhash") String passhash) {

        AuthenticationResult authenticationResult = new AuthenticationResult();

        HttpSession session = request.getSession();
        String hashkey = (String) session.getAttribute(HASH_KEY_SESSION_KEY);

        boolean credentialsValid = authenticationBean.credentialsValid(username, passhash, hashkey);
        if (credentialsValid) {
            authenticationResult.loggedIn = true;
            long currentTimeMillis = System.currentTimeMillis();
            session.setAttribute(AUTH_TILL_SESSION_KEY, currentTimeMillis + TimeUnit.MINUTES.toMillis(30));
            session.setAttribute(USERNAME_SESSION_KEY, username);
        } else {
            authenticationResult.loggedIn = false;
        }

        return authenticationResult;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public AuthenticationResult isAuthenticated(@Context HttpServletRequest request) {

        AuthenticationResult authenticationResult = new AuthenticationResult();

        HttpSession session = request.getSession();
        Long authTill = (Long) session.getAttribute(AUTH_TILL_SESSION_KEY);
        if (authTill != null) {
            if (authTill < System.currentTimeMillis()) {
                session.removeAttribute(AUTH_TILL_SESSION_KEY);
                session.removeAttribute(USERNAME_SESSION_KEY);
                authenticationResult.loggedIn = false;
            } else {
                authenticationResult.loggedIn = true;
            }
        } else {
            authenticationResult.loggedIn = false;
        }

        return authenticationResult;
    }

    public class HashKey {
        public String key;
    }

    @GET
    @Path("hashkey")
    @Produces(MediaType.APPLICATION_JSON)
    public HashKey getToken(@Context HttpServletRequest request) {
        HttpSession session = request.getSession();
        HashKey hashKey = new HashKey();
        hashKey.key = UUID.randomUUID().toString().replace("-", "");
        session.setAttribute(HASH_KEY_SESSION_KEY, hashKey.key);
        return hashKey;
    }
}
