package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.DomainDao;

import javax.inject.Inject;

public class DomainResolutionBean {

    @Inject
    DatabaseBean databaseBean;

    public boolean isInternalDomainRegistered(String shopInternalDomain) {
        return false;
    }

    public boolean isDomainRegistered(String httpHost) {
        DomainDao domainDao = databaseBean.getDomainDao();
        return domainDao.isDomainRegistered(httpHost);
    }

    public String resolveBasicUrl(String domain, String urlCall) {
        return "/api/auth";
    }
}
