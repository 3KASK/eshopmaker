package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api.shop;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * API for shop configuration
 */
@Path("shop/{shop}/config")
public class ShopConfigurationApi {

    @PathParam("shop")
    private String shop;
}
