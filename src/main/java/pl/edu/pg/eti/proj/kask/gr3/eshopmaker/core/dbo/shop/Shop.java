package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.access.Account;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Shop extends IdEntity {

    public String name;

    @ManyToOne
    public Account owner;
}
