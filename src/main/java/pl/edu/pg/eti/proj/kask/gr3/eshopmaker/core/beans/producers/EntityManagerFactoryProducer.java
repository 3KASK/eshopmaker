package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.producers;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.HomeDirectoryBean;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class EntityManagerFactoryProducer {

    @Inject
    HomeDirectoryBean homeDirectoryBean;

    private EntityManagerFactory entityManagerFactory;

    @Produces
    public EntityManagerFactory produceEntityManagerFactory() {
        if (entityManagerFactory == null) {
            String homePath = homeDirectoryBean.getHomePath();

            Map<String, Object> configOverrides = new HashMap<String, Object>();
            configOverrides.put("hibernate.connection.url", "jdbc:h2:" + homePath + "/eshopmaker.h2db");
            entityManagerFactory = Persistence.createEntityManagerFactory("h2", configOverrides);
        }
        return entityManagerFactory;
    }
}
