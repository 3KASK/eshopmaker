package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.File;

@Startup
public class HomeDirectoryBean {

    @Inject
    private Logger logger;

    private String homeDirectoryAbsolutePath;

    @PostConstruct
    public void postConstruct() {
        logger.info("constructed!");

        homeDirectoryAbsolutePath = System.getProperty("eshopmaker.home", System.getProperty("user.dir") + "/eshopmaker");

        if (getDirectory("") == null) {
            System.exit(-1);
        }
    }

    public String getHomePath() {
        return homeDirectoryAbsolutePath;
    }

    public File getDirectory(String pathInHome) {
        if (!pathInHome.isEmpty() && !pathInHome.startsWith("/")) {
            pathInHome = "/" + pathInHome;
        }
        File dir = new File(homeDirectoryAbsolutePath + pathInHome);

        if (dir.exists() && !dir.isDirectory()) {
            return null;
        } else if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }
}
