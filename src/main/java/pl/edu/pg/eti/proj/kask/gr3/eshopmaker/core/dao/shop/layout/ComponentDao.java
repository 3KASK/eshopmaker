package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.layout;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.AbstractDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.layout.Component;

public class ComponentDao extends AbstractDao<Component> {
}
