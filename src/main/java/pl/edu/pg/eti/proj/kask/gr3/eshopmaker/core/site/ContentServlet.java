package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ContentServlet", value = "/content/*")
public class ContentServlet extends HttpServlet {

    @Inject
    WebsiteContent websiteContent;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        websiteContent.render(request, response);
    }
}
