package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.access.AccountDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.ShopDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Shop;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * API for platform user management (don't mistake with shop customer)
 *
 * @see pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.api.shop.ShopAccountApi
 */
@Path("user")
public class AccountApi {

    @Inject
    private AccountDao accountDao;

    @Inject
    private ShopDao shopDao;

    @GET
    @Path("{username}")
    public String userProfile(@PathParam("username") String name) {
        return "profile";
    }

    public class AccountCreationResult {
        public boolean accountCreated;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AccountCreationResult createAccount(
            @FormParam("username") String username,
            @FormParam("password") String password) {
        AccountCreationResult accountCreationResult = new AccountCreationResult();

        if (accountDao.canCreateAccount(username)) {
            accountCreationResult.accountCreated = accountDao.createAccount(username, password);
        } else {
            accountCreationResult.accountCreated = false;
        }

        return accountCreationResult;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class ShopEntry {
        public String id;
        public String shopName;
    }

    @POST
    @Path("{username}/shop")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ShopEntry createShop(
            @Context HttpServletRequest request,
            @PathParam("username") String username,
            ShopEntry shopEntry) {

        Shop newShop = new Shop();
        newShop.name = shopEntry.shopName;
        shopDao.save(newShop);

        shopEntry.shopName = newShop.name;
        shopEntry.id = newShop.id;

        return shopEntry;
    }

    @GET
    @Path("{username}/shops")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ShopEntry> getShops(@PathParam("username") String username) {
        List<ShopEntry> shopEntryList = new ArrayList<>();

        List<Shop> shopList = shopDao.getAll(Shop.class);

        for (Shop shop : shopList) {
            ShopEntry shopEntry = new ShopEntry();
            shopEntry.id = shop.id;
            shopEntry.shopName = shop.name;
            shopEntryList.add(shopEntry);
        }

        return shopEntryList;
    }
}