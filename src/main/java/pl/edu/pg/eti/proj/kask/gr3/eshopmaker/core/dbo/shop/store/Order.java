package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.store;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Customer;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.Shop;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Order extends IdEntity {

    @ManyToOne
    public Shop shop;

    @ManyToOne
    public Customer customer;
}
