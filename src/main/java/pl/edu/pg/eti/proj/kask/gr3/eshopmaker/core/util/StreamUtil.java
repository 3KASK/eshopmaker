package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class StreamUtil {

    public static void writeFileToOutputStream(String filePath, OutputStream out) throws IOException {
        FileInputStream in = new FileInputStream(filePath);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
    }
}
