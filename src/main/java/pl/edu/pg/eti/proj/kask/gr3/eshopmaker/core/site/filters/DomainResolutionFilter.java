package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site.filters;

import org.apache.log4j.Logger;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans.DomainResolutionBean;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * We always forward with this filter!
 * Other filters should have type {@code javax.servlet.DispatcherType#FORWARD}
 * and no type {@code javax.servlet.DispatcherType#REQUEST}
 * This filter should be only with type {@code javax.servlet.DispatcherType#REQUEST}
 *
 * @see WebFilter#dispatcherTypes()
 */
@WebFilter(urlPatterns = "/*")
public class DomainResolutionFilter implements Filter {

    @Inject
    DomainResolutionBean domainResolutionBean;

    @Inject
    Logger logger;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String httpHost = request.getServerName();

        if (domainResolutionBean.isDomainRegistered(httpHost)) {
            String basicUrl = domainResolutionBean.resolveBasicUrl(httpHost, httpServletRequest.getPathInfo());
            request.getRequestDispatcher(basicUrl).forward(request, response);
        } else {
            chain.doFilter(request, response);
            //request.getRequestDispatcher(httpServletRequest.getPathInfo()).forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
