package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo;

import javax.persistence.Entity;

@Entity
public class FileRef extends IdEntity {

    public String url;
}
