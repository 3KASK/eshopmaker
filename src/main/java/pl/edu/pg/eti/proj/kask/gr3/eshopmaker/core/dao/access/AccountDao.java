package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.access;

import org.apache.log4j.Logger;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.AbstractDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.access.Account;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class AccountDao extends AbstractDao<Account> {

    @Inject
    private Logger logger;

    public interface HashMechanism {
        String hash(String base);
    }

    public boolean accountExists(String username) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Long> query = entityManager.createQuery(
                    "select count(acc.username) from Account acc where acc.username = :username", Long.class);

            boolean result = query.setParameter("username", username).getSingleResult() != 0;
            return result;
        } finally {
            entityManager.close();
        }
    }

    public boolean canCreateAccount(String username) {

        try {
            return !accountExists(username);
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean createAccount(String username, String password) {

        if (canCreateAccount(username)) {
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            Account account = new Account();
            account.username = username;
            account.password = password;

            entityManager.getTransaction().begin();
            entityManager.persist(account);
            entityManager.getTransaction().commit();

            entityManager.close();
            return true;
        } else {
            return false;
        }
    }

    public boolean credentialsValid(String username, String passhash, HashMechanism hasher) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        try {
            TypedQuery<String> query = entityManager.createQuery(
                    "select acc.password from Account acc where acc.username = :username", String.class);
            String password = query.setParameter("username", username).getSingleResult();
            return hasher.hash(password).equals(passhash);
        } catch (NoResultException e) {
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return false;
    }
}
