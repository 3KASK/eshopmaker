package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.ContentDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.FileRefDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.PropertyDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.access.AccountDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.access.ShopPermissionDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.CustomerDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.DomainDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.ShopDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.layout.ComponentDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.store.ItemCommentDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.store.ItemDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.store.OrderDao;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao.shop.store.OrderEntryDao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Used to maintain database connection
 * Stores all DAO objects
 */
@ApplicationScoped
public class DatabaseBean {

    @Inject
    private AccountDao accountDao;

    @Inject
    private ShopPermissionDao permissionLevelDao;

    @Inject
    private ComponentDao componentDao;

    @Inject
    private ItemCommentDao itemCommentDao;

    @Inject
    private ItemDao itemDao;

    @Inject
    private OrderDao orderDao;

    @Inject
    private OrderEntryDao orderEntryDao;

    @Inject
    private CustomerDao customerDao;

    @Inject
    private DomainDao domainDao;

    @Inject
    private ShopDao shopDao;

    @Inject
    private ContentDao contentDao;

    @Inject
    private FileRefDao fileRefDao;

    @Inject
    private PropertyDao propertyDao;

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public ShopPermissionDao getPermissionLevelDao() {
        return permissionLevelDao;
    }

    public ComponentDao getComponentDao() {
        return componentDao;
    }

    public ItemCommentDao getItemCommentDao() {
        return itemCommentDao;
    }

    public ItemDao getItemDao() {
        return itemDao;
    }

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public OrderEntryDao getOrderEntryDao() {
        return orderEntryDao;
    }

    public CustomerDao getCustomerDao() {
        return customerDao;
    }

    public DomainDao getDomainDao() {
        return domainDao;
    }

    public ShopDao getShopDao() {
        return shopDao;
    }

    public ContentDao getContentDao() {
        return contentDao;
    }

    public FileRefDao getFileRefDao() {
        return fileRefDao;
    }

    public PropertyDao getPropertyDao() {
        return propertyDao;
    }
}
