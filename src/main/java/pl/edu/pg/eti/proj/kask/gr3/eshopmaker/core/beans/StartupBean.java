package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

public class StartupBean {

    @Inject
    private HomeDirectoryBean homeDirectoryBean;

    @Inject
    private DatabaseBean databaseBean;

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {

    }

    public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) {

    }
}
