package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site.resource;

import com.google.template.soy.SoyToJsSrcCompiler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TemplateResourceServlet", value = "/resource/template/*")
public class TemplateResourceServlet extends HttpServlet {
    SoyToJsSrcCompiler soyToJsSrcCompiler;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
