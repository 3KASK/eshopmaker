package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.shop.store;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.Content;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class OrderEntry extends IdEntity {

    @ManyToOne
    public Order order;

    @ManyToOne
    public Item item;

    public int count;

    @ManyToOne
    public Content message;
}
