package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class IdEntity {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    public String id;

    public IdEntity() {
    }

    public IdEntity(String id) {
        this.id = id;
    }
}
