package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.beans;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Used to manage uploaded files as well as generated sources and resource
 */
@ApplicationScoped
public class FileStorageBean {

    @Inject
    private Logger logger;

    @Inject
    private HomeDirectoryBean homeDirectoryBean;

    @PostConstruct
    public void postConstruct() {
        logger.info("constructed!");
    }
}
