package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util;

import java.io.File;
import java.net.URL;

public final class ResourceUtil {

    public static File getFile(String resourcePath) {
        String absolutePath = getAbsolutePath(resourcePath);
        if (!absolutePath.isEmpty()) {
            return new File(absolutePath);
        } else {
            return null;
        }
    }

    public static String getAbsolutePath(String resourcePath) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(resourcePath);
        if (resource != null) {
            return resource.getPath();
        } else {
            return "";
        }
    }
}
