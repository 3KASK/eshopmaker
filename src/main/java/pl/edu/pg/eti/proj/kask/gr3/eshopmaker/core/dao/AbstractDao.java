package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dao;

import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.dbo.IdEntity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractDao<DbObject extends IdEntity> {

    @Inject
    protected EntityManagerFactory entityManagerFactory;

    public interface EntityManagerTask<Type> {
        Type task(EntityManager entityManager);
    }

    public Object execute(EntityManagerTask entityManagerTask) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Object result = entityManagerTask.task(entityManager);
        entityManager.close();
        return result;
    }

    public void save(DbObject object) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(object);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public DbObject load(String id, Class<DbObject> cla55) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        DbObject result = entityManager.getReference(cla55, id);
        entityManager.close();
        return result;
    }

    public List<DbObject> getAll(Class<DbObject> cla55) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DbObject> criteriaQuery = criteriaBuilder.createQuery(cla55);
        List<DbObject> resultList =
                entityManager.createQuery(
                        criteriaQuery.select(criteriaQuery.from(cla55))
                ).getResultList();

        entityManager.close();

        return resultList;
    }

}
