package pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.site;

import com.google.common.collect.ImmutableMap;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import pl.edu.pg.eti.proj.kask.gr3.eshopmaker.core.util.ResourceUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class WebsiteContent {

    private SoyMapData getSoyInjectedData() {
        SoyMapData ijData = new SoyMapData();
        ijData.put("scripts", new SoyMapData(
            "internal", new SoyListData(
                //"/resource/js/test.js",
                "/resource/js/md5.js",
                "/resource/js/jquery.min.js",
                "/resource/js/kendo/kendo.all.min.js"
            ),
            "external", new SoyListData(

            )
        ));
        ijData.put("stylesheets", new SoyMapData(
            "internal", new SoyListData(
                "/resource/style/test.css",
                "/resource/style/kendo/kendo.common.min.css",
                "/resource/style/kendo/kendo.default.min.css"
            ),
            "external", new SoyListData(

            )
        ));
        return ijData;
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Map<String, Object> data = ImmutableMap.<String, Object>of(
                "context", request.getContextPath()
        );

        SoyFileSet soyFileSet = SoyFileSet.builder().add(
                ResourceUtil.getFile("templates/eshopmakerBaseHtml.soy")
        ).setCompileTimeGlobals(data).build();

        response.getWriter().print(soyFileSet.compileToTofu().newRenderer("base.html")
                .setIjData(getSoyInjectedData()).render());
    }
}
